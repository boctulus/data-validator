<?php

use PHPUnit\Framework\TestCase;

require 'validator.php';


final class ValidatorTest extends TestCase
{   
	public function testValidarEmptyRuleException()
    {
        $this->expectException(InvalidArgumentException::class);
        Validator::validar([],['username'=>'admin','correo'=>'admin@serviya.com']);
    }

	public function testValidarEmptyDataException()
    {
        $this->expectException(InvalidArgumentException::class);
        Validator::validar([['campo'=>'username','requerido'=>true],
							['campo'=>'correo','requerido'=>false]],[]);
    }
	
    public function testValidar()
    {
		// caso base, no hay "reglas" => no se valida nada
		$this->assertTrue(
            Validator::validar(
			[
				['campo'=>'username'],
				['campo'=>'correo'],
			],
			[
				'username'=>'admin',
				'correo'=>'admin@serviya.com'
			])
        );
		
		// campos requeridos
		$this->assertTrue(
            Validator::validar(
			[
				['campo'=>'username','requerido'=>true],
				['campo'=>'correo','requerido'=>false],
			],
			[
				'username'=>'admin',
				'correo'=>'admin@serviya.com'
			])
        );
		
		// si la validacion falla, la salida es un array con los errores encontrados
		$this->assertInternalType('array',
            Validator::validar(
			[
				['campo'=>'username','requerido'=>true], // vacio
				['campo'=>'correo','requerido'=>false],
			],
			[
				'username'=>'',
				'correo'=>'admin@serviya.com'
			])
        );
		
		$this->assertInternalType('array',Validator::validar(
			[
				['campo'=>'username'],
				['campo'=>'correo','tipo'=>'string','max'=>'30'],
			],
			[
				'username'=>'admin',
				'correo'=>'administradormundialdelmundoconocidouniversal@serviya.com'
			]));
		
		$this->assertInternalType('array',Validator::validar(
			[
				['campo'=>'nombre','tipo'=>'string no-numerico']
			],
			
				[
				'nombre'=>'Sebastian2'
				]
			));	
			
		$this->assertInternalType('array',Validator::validar(
			[
				['campo'=>'fuerza','tipo'=>'integer','min'=>'30']
			],
			
				[
				'fuerza'=>''
				]
			));		
		
		// valido IPv4 con regex, formato regex:/expresion/	
		// (hay otras formas de validarlas, es solo un ejemplo de uso)
		$this->assertTrue(Validator::validar(
			[
				['campo'=>'IPv4','tipo'=>'regex:/^((2[0-4]|1\d|[1-9])?\d|25[0-5])(\.(?1)){3}\z/']
			],
				[
				'IPv4'=>'192.168.0.27'	
				]
			));	
			
		$this->assertInternalType('array',Validator::validar(
			[
				['campo'=>'IPv4','tipo'=>'regex:/^((2[0-4]|1\d|[1-9])?\d|25[0-5])(\.(?1)){3}\z/']
			],
				[
					'IPv4'=>'192.168.0.300'
				]
			));	
	
		$this->assertInternalType('array',Validator::validar(
			[
				['campo'=>'frutas_favoritas','tipo'=>'array','min'=>3]
			],
				[
					'frutas_favoritas'=>['bananas','manzanas']
				]
			));	
			
			$this->assertTrue(Validator::validar(
			[
				['campo'=>'frutas_favoritas','tipo'=>'array','min'=>3]
			],
				[
					'frutas_favoritas'=>['bananas','manzanas','peras']
				]
			));		
	
    }
	
	public function testEsTipoDatoExceptionPorTipoEmpty()
    {
        $this->expectException(InvalidArgumentException::class);
        Validator::esTipoDato('Brayan','');
    }
	
	public function testEsTipoDatoExceptionPorTipoDesconocido()
    {
        $this->expectException(InvalidArgumentException::class);
        Validator::esTipoDato('Brayan','entero');
    }
	
	public function testEsTipoDatoExceptionPorDatoNull()
    {
        $this->expectException(InvalidArgumentException::class);
        Validator::esTipoDato(NULL,'int');
    }
	
	public function testEsTipoDatoExceptionPorRegexInvalida()
    {
        $this->expectException(InvalidArgumentException::class);
        Validator::esTipoDato('190-200-200','regex:/(.*/');
    }
	
	public function testEsTipoDato()
    {
		$this->assertTrue(
			Validator::esTipoDato('Brayan','string')
        );
		
		$this->assertFalse(
			Validator::esTipoDato('1250','string no-numerico')
        );
		
		$this->assertFalse(
			Validator::esTipoDato('Sebastian2','string no-numerico')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('1250','string')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('','string')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('   ','string')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('32543','numeric')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('-32543','numeric')
        );
		
		$this->assertTrue(
			Validator::esTipoDato(' 16  ','numeric')
        );
		
		$this->assertTrue(
			Validator::esTipoDato(' 16  ','int')
        );
		
		$this->assertFalse(
			Validator::esTipoDato('16.3','int')
        );
		
		$this->assertFalse(
			Validator::esTipoDato(' 16.3  ','int')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('16.3','decimal')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('-.023','decimal')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('.023','numeric')
        );
		
		$this->assertTrue(
			Validator::esTipoDato('192.168.0.27','regex:/^((2[0-4]|1\d|[1-9])?\d|25[0-5])(\.(?1)){3}\z/')
        );
		
		$this->assertFalse(
			Validator::esTipoDato('192.168.0.300','regex:/^((2[0-4]|1\d|[1-9])?\d|25[0-5])(\.(?1)){3}\z/')
        );
	}
	
	
}
