<?php

require 'validator.php';

/*
	Ejemplo de uso
*/

$data = [
			'nombre'=>'Sebastian1',
			'apellido'=>'Be',
			'usuario'=>'',
			'celular'=>'32153', 
			'correo'=>'a@b',
			'calle'=>'0',
			'numero de casa'=>'',
			'observaciones'=>'la vida es complicada y bla bla bla bla bla bla bla',
			'fecha'=>'32-09-2019',
			'hora'=>'24:00:17',
			'rol'=>'',
			'fuerza'=>'100.xxx',
			'estrato'=>'3',
			'felicidad'=>'0.25',
			'energia'=>'.25',
			'hora_almuerzo'=>'13:30:00',
			'hora_cena'=>'18:00:00',
			'fecha_nac'=>'10-12-1902',
			'frutas_favoritas'=>['bananas','manzanas']  // podria provenir de un grupo de checkboxes
			
];

$rules = [
			['campo'=>'nombre','tipo'=>'string no-numerico','requerido'=>true],
			['campo'=>'apellido','tipo'=>'string no-numerico','requerido'=>true,'min'=>3,'max'=>30],
			['campo'=>'usuario','requerido'=>true,'min'=>2,'max'=>15],
			['campo'=>'celular','tipo'=>'regex:/^[0-9]{10}$/','requerido'=>true],
			['campo'=>'correo','tipo'=>'email','requerido'=>true], 
			['campo'=>'calle','tipo'=>'int','requerido'=>false, 'min'=>1],
			['campo'=>'numero de casa','tipo'=>'numeric','requerido'=>false],
			['campo'=>'observaciones','tipo'=>'string','max'=>40],
			['campo'=>'fecha','tipo'=>'date'], 
			['campo'=>'hora','tipo'=>'time'], 
			['campo'=>'rol','tipo'=>'int','requerido'=>false], 
			['campo'=>'fuerza','tipo'=>'decimal','requerido'=>false],
			['campo'=>'estrato','tipo'=>'int','requerido'=>false, 'min'=>1, 'max'=>6],
			['campo'=>'felicidad','tipo'=>'int','requerido'=>false, 'min'=>0, 'max'=>100],
			['campo'=>'energia','tipo'=>'decimal','requerido'=>false, 'min'=>0, 'max'=>100],
			['campo'=>'hora_almuerzo','tipo'=>'time','min'=>'11:00:00','max'=>'10:15:00'],
			['campo'=>'hora_cena','tipo'=>'time','min'=>'19:00:00','max'=>'22:30:00'],
			['campo'=>'fecha_nac','tipo'=>'date','min'=>'01-01-1980','max'=>'12-12-2018'],
			['campo'=>'frutas_favoritas','tipo'=>'array','min'=>3]
			
];

// Salida: true | array de errores encontrados
// el tercer parametro es opcional y es un array de campos que quiero ignorar pero sin borrar las reglas para ese campo.
var_dump(Validator::validar($rules,$data));



