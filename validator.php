<?php

/*
	Validador de campos de formulario
	Ver 2.0 Beta

	@author boctulus
	
	Novedad: el desacople de reglas y datos
*/
class Validator
{
	/*
		@param string $dato
		@param string $tipo
		@return mixed
		
		El tipo de dato es obligatorio (no nulo)
		
		Los	tipos admitidos son	'int' | 'decimal' | 'numeric' | 'string' | 'string no-numerico' |
		'email' | 'date' | 'time' | 'regex:/expresion/' y sus alias.
		
		Se ha considerado innecesario un tipo boolean
	*/
	static function esTipoDato($dato, $tipo){
		if ($dato === NULL)
			throw new InvalidArgumentException('El dato no puede ser NULL'); 
		
		if (empty($tipo))
			throw new InvalidArgumentException('No se ha especificado un tipo de dato');

		if ($tipo == 'int' || $tipo == 'integer'){
			return preg_match('/^(-?[0-9]+)$/',trim($dato)) == 1;
		}elseif($tipo == 'decimal' || $tipo == 'float' || $tipo == 'double'){
			$dato = trim($dato);
			return is_numeric($dato);
		}elseif($tipo == 'numeric' || $tipo == 'number'){
			$dato = trim($dato);
			return ctype_digit($dato) || is_numeric($dato);
		}elseif($tipo == 'string' || $tipo == 'str'){
			return is_string($dato);	
		}elseif($tipo == 'string no-numerico' || $tipo == 'str no-numerico'){
			return preg_match('/[0-9]+/',$dato) == 0;
		}elseif($tipo == 'email' || $tipo == 'correo'){
				return filter_var($dato, FILTER_VALIDATE_EMAIL);
		}elseif($tipo == 'date'){
				return get_class()::isValidDate($dato);
		}elseif($tipo == 'time'){
				return get_class()::isValidDate($dato,'H:i:s');	
		// formato: 'regex:/expresion/'			
		}elseif ((substr($tipo,0,7)=='regex:/') /* && (substr($tipo,-1)=='/') */ ){
			try{
				$regex = substr($tipo,6);
				return preg_match($regex,$dato) == 1;	
			}catch(Exception $e){
				throw new InvalidArgumentException('La regex es invalida!');
			}	
		}elseif($tipo == 'array'){
				return is_array($dato);			
		}else
			throw new InvalidArgumentException('Tipo de dato no reconocido!');
	}	
	
	/*
		@param array $rules
		@param array $data
		@param array $ignored_fields
		@return mixed
		
		$rules es un array de arrays con las keys: dato,'tipo?,requerido?
	*/
	static function validar(array $rules, array $data, array $ignored_fields = []){
		if (empty($rules))
			throw new InvalidArgumentException('No hay validaciones!');
		
		if (empty($data))
			throw new InvalidArgumentException('No hay datos!');
	

		$errores = [];
		
		/*
			Crea array con el campo como índice
		*/
		$push_error = function ($campo, array $error, array &$errores){
			if(isset($errores[$campo]))
				$errores[$campo][] = $error;
			else{
				$errores[$campo] = [];
				$errores[$campo][] = $error;
			}	
		};
			
		foreach($rules as $rule){
			
			// Esto permite que un campo pueda no estar presente pero 
			// lamentablemente puede silenciar errores
			if (!isset($data[$rule['campo']]))
				continue;
			
			$dato = $data[$rule['campo']];
			
			if(in_array($rule['campo'],$ignored_fields))
				continue;
			
			if (!isset($rule['requerido']))
				$rule['requerido'] = false;

			$avoid_type_check = false;
			if($rule['requerido']){
				if(trim($dato)=='')
					$push_error($rule['campo'],['dato'=>$dato, 'error'=>'requerido', 'error_msg' =>$rule['campo'].' es requerido'],$errores);
			}	
			
			if (isset($rule['tipo']) && in_array($rule['tipo'],['numeric','number','int','integer','float','double','decimal']) && trim($dato)=='')
				$avoid_type_check = true;
			
			if (isset($rule['tipo']) && !$avoid_type_check)
				if (!get_class()::esTipoDato($dato, $rule['tipo']))
					$push_error($rule['campo'],['dato'=>$dato, 'error'=>'tipo', 'error_msg' => "no es {$rule['tipo']}"],$errores);
				
					
			if(isset($rule['tipo'])){	
				if(in_array($rule['tipo'],['str','string','string no-numerico','str no-numerico','email'])){
						
						if(isset($rule['min'])){ 
							$rule['min'] = (int) $rule['min'];
							if(strlen($dato)<$rule['min'])
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'min', 'error_msg' => 'la longitud mínima es de '.$rule['min']],$errores);
						}
						
						if(isset($rule['max'])){ 
							$rule['max'] = (int) $rule['max'];
							if(strlen($dato)>$rule['max'])
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'max', 'error_msg' => 'la longitud maxima es de '.$rule['max']],$errores);
						}
				}	
				
				if(in_array($rule['tipo'],['numeric','number','int','integer','float','double','decimal'])){
						
						if(isset($rule['min'])){ 
							$rule['min'] = (int) $rule['min'];
							if($dato<$rule['min'])
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'min', 'error_msg' => 'el mínimo es de '.$rule['min']],$errores);
						}
						
						if(isset($rule['max'])){ 
							$rule['max'] = (int) $rule['max'];
							if($dato>$rule['max'])
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'max', 'error_msg' => 'el maximo es de '.$rule['max']],$errores);
						}
				}	
				
				if(in_array($rule['tipo'],['time','date'])){
						
						if(isset($rule['min'])){ 
							if(strtotime($dato)<strtotime($rule['min']))
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'min', 'error_msg' => 'mínimo '.$rule['min']],$errores);
						}
						
						if(isset($rule['max'])){ 
							if(strtotime($dato)>strtotime($rule['max']))
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'max', 'error_msg' => 'maximo '.$rule['max']],$errores);
						}
				}	
				
				if($rule['tipo']=='array' && is_array($dato)){
						$rule['min'] = (int) $rule['min'];
						if(isset($rule['min'])){ 
							if(count($dato)<$rule['min'])
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'min', 'error_msg' => 'mínimo '.$rule['min'].' opciones'],$errores);
						}
						
						if(isset($rule['max'])){ 
						$rule['max'] = (int) $rule['max'];
							if(count($dato)>$rule['max'])
								$push_error($rule['campo'],['dato'=>$dato, 'error'=>'min', 'error_msg' => 'máximo '.$rule['max'].' opciones'],$errores);
						}
				}
				
			}	
				
		}
		return empty($errores) ? true : get_class()::humanizarErrores($errores);
	}
	
	
	private static function humanizarErrores(array $errores){
		$reemplazos = [
						'no es str no-numerico' => 'no se permiten números',
						'no es string no-numerico' => 'no se permiten números',
						'no es email' => 'correo no válido',
						'no es correo' => 'correo no válido',
						'no es date' => 'fecha inválida',
						'no es time' => 'hora inválida',
						'no es number' => 'no es un número',
						'no es numeric' => 'no es un número',
						'no es decimal' => 'no es un número',
						'no es double' => 'no es un número',
						'no es float' => 'no es un número',
						'no es int' => 'no es un número válido'
					];	
					
		
		foreach($errores as $campo => $rows){
			foreach($rows as $ix => $row){
				foreach($reemplazos as $orginal => $reemplazo)
					if ($row['error_msg'] == $orginal)
						$errores[$campo][$ix]['error_msg'] = $reemplazo;
					
					if (strstr($row['error_msg'],'regex:/'))
						$errores[$campo][$ix]['error_msg'] = 'inválido';
			}
		}
			
		return $errores;
	}
	
	private static function isValidDate($date, $format = 'd-m-Y') {
		$dateObj = DateTime::createFromFormat($format, $date);
		return $dateObj && $dateObj->format($format) == $date;
	}
}


/*
	Helper
*/


// @author: vitalyart dot ru
if (!function_exists('array_key_first')) {
    /**
     * Gets the first key of an array
     *
     * @param array $array
     * @return mixed
     */
    function array_key_first(array $array)
    {
        if (count($array)) {
            reset($array);
            return key($array);
        }

        return null;
    }
}



